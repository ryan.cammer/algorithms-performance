# Algorithms Performance
The Algorithms Performance project provides a harness for testing the performance of
various algorithms.

## Getting Started

### Prerequisites
* [Python](https://www.python.org/)
* [Pipenv](https://pipenv.pypa.io/en/latest/)

### Steps
1. Clone this repo using ssh: `git clone git@gitlab.com:ryan.cammer/algorithms-performance.git`
2. Install the required Python libraries in the project root directory: `pipenv install`
3. Run the harness: `pipenv run python main.py`

## TODO
- [ ] "Algorithm Sorting did NOT produce the expected result." with a large number of elements. Determine why and fix.
- [ ] Implement a command line option to specify which algorithms to run.
- [ ] Implement a command line option to specify how many lists to test.
- [ ] Implement a command line option to specify how many iterations to run.
- [ ] Implement a command line option to list the available tests (currently `find_three_elements_who_sum_to_zero.py`) and their corresponding algorithms.
- [ ] Why is bisect so slow? It should be an O(log n) operation, but the Sorting algorithm is faster by an order of magnitude.
