from algorithms.find_three_elements_who_sum_to_zero import BruteForceElementFinder, SortingElementFinder, \
    BisectingSortingElementFinder, HashingElementFinder
from performance.harness import TestHarness, TestElements


TestHarness(
    [
        # Brute force takes too long! It's O(n^3) and the performance blows right up.
        # BruteForceElementFinder(),
        SortingElementFinder(),
        # This is also taking way too long, although the performance is still much faster
        # than the brute force implementation.
        # BisectingSortingElementFinder(),
        HashingElementFinder()
    ]
).test(
    element_lists=[
        TestElements() for _ in range(100)
    ],
    number_of_iterations=10
)
