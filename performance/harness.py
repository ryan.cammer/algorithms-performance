import secrets
import time


class TestHarness:
    def __init__(self, finders):
        self._finders = finders

    @property
    def finders(self):
        return self._finders

    def test(self, element_lists, number_of_iterations):
        for finder in self.finders:
            start_time = time.perf_counter()

            for i in range(number_of_iterations):
                for element_list in element_lists:
                    result = finder.find(element_list.elements)
                    if result != element_list.contains_3_zeroing_elements:
                        print(f'Algorithm {finder.name} did NOT produce the expected result.')

            total_time = time.perf_counter() - start_time

            print(f'{finder.name} took {total_time} s for {number_of_iterations * len(element_lists)} iterations.')


class TestElements:
    MAX_ELEMENTS = 2 ** 16

    def __init__(self, element_count=None):
        if element_count is None:
            element_count = secrets.randbelow(self.MAX_ELEMENTS)

        self._elements = [
            secrets.randbelow(2 ** 32) * secrets.choice([-1, 1])
            for _ in range(element_count)
        ]

        self._first_index = None
        self._second_index = None
        self._third_index = None
        self._first_value = None
        self._second_value = None
        self._third_value = None

        self._contains_3_zeroing_elements = secrets.choice([-1, 1]) == 1

        if self._contains_3_zeroing_elements:
            self._first_index = secrets.randbelow(element_count)

            self._second_index = self._first_index

            while self._second_index == self._first_index:
                self._second_index = secrets.randbelow(element_count)

            self._third_index = self._second_index

            while self._third_index == self._second_index or self._third_index == self._first_index:
                self._third_index = secrets.randbelow(element_count)

            self._first_value = secrets.randbelow(2 ** 32)

            self._second_value = secrets.randbelow(2 ** 32)

            self._third_value = -1 * (self._first_value + self._second_value)

            self._elements.insert(self._first_index, self._first_value)
            self._elements.insert(self._second_index, self._second_value)
            self._elements.insert(self._third_index, self._third_value)

    @property
    def contains_3_zeroing_elements(self):
        return self._contains_3_zeroing_elements

    @property
    def first_index(self):
        return self._first_index

    @property
    def second_index(self):
        return self._second_index

    @property
    def third_index(self):
        return self._third_index

    @property
    def first_value(self):
        return self._first_value

    @property
    def second_value(self):
        return self._second_value

    @property
    def third_value(self):
        return self._third_value

    @property
    def elements(self):
        return self._elements
