import bisect


class BruteForceElementFinder:
    @property
    def name(self):
        return "Brute Force"

    def find(self, elements):
        elements_length = len(elements)

        for i in range(elements_length):
            ith_element = elements[i]

            for j in range(i + 1, elements_length):
                jth_element = elements[j]

                for k in range(j + 1, elements_length):
                    kth_element = elements[k]

                    if ith_element + jth_element + kth_element == 0:
                        return True

        return False


class SortingElementFinder:
    @property
    def name(self):
        return "Sorting"

    def find(self, elements):
        sorted_elements = sorted(elements)

        if sorted_elements[0] > 0 or sorted_elements[-1] < 0:
            return False

        sorted_elements_length = len(sorted_elements)

        for i in range(len(sorted_elements)):
            sorted_ith_element = sorted_elements[i]

            if sorted_ith_element > 0:
                return False

            negative_ith_value = -sorted_ith_element
            start = i + 1
            end = sorted_elements_length - 1

            if sorted_elements_length - i - 1 > 4:
                if (sorted_elements[start] + sorted_elements[start + 1]) > negative_ith_value or (
                        sorted_elements[end] + sorted_elements[end - 1]) < negative_ith_value:
                    continue

            while start < end:
                j_plus_k = sorted_elements[start] + sorted_elements[end]
                if j_plus_k == negative_ith_value:
                    return True
                elif j_plus_k < negative_ith_value:
                    start += 1
                else:
                    end -= 1

        return False


class BisectingSortingElementFinder:
    @property
    def name(self):
        return "Bisecting Sorting"

    def _in_sorted_list(self, elem, sorted_list):
        i = bisect.bisect_left(sorted_list, elem)
        return i != len(sorted_list) and sorted_list[i] == elem

    def find(self, elements):
        sorted_elements = sorted(elements)

        sorted_elements_length = len(sorted_elements)

        for i in range(len(sorted_elements)):
            if sorted_elements[i] > 0:
                return False

            for j in range(i + 1, sorted_elements_length):
                value = -1 * (sorted_elements[i] + sorted_elements[j])

                if self._in_sorted_list(value, sorted_elements[j + 1::]):
                    return True

        return False


class HashingElementFinder:
    @property
    def name(self):
        return "Hashing"

    def find(self, elements):
        element_length = len(elements)

        for i in range(element_length):
            ith_element = elements[i]

            negative_sums: dict[int, bool] = {}

            for j in range(i + 1, element_length):
                jth_element = elements[j]

                negative_sum = -(ith_element + jth_element)

                if negative_sum in negative_sums:
                    return True
                else:
                    negative_sums[jth_element] = True

        return False
